-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: girlz
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `girlz`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `girlz` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `girlz`;

--
-- Table structure for table `girls`
--

DROP TABLE IF EXISTS `girls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `girls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `age` int(150) NOT NULL,
  `city` varchar(100) NOT NULL,
  `weight` int(255) NOT NULL,
  `height` int(255) NOT NULL,
  `hcolor` varchar(50) NOT NULL,
  `ecolor` varchar(50) NOT NULL,
  `tel` varchar(17) NOT NULL,
  `email` varchar(90) NOT NULL,
  `info` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `girls`
--

LOCK TABLES `girls` WRITE;
/*!40000 ALTER TABLE `girls` DISABLE KEYS */;
INSERT INTO `girls` VALUES (1,'Peter',33,'minks',111,111,'red','brown','7834783223','mail@Ma.ru','text nah'),(2,'Noble Black',26,'Animi incididunt vo',28,55,'Est quis laboriosam','Irure delectus cons','+1 (492) 268-8216','jocilame@mailinator.com','Et sequi et neque se'),(3,'Xerxes Osborne',15,'Molestias obcaecati ',7,39,'Voluptatem et et sun','Perspiciatis adipis','+1 (853) 402-6685','juvef@mailinator.com','Eveniet nulla tempo'),(4,'Xerxes Osborne',15,'Molestias obcaecati ',7,39,'Voluptatem et et sun','Perspiciatis adipis','+1 (853) 402-6685','juvef@mailinator.com','Eveniet nulla tempo'),(5,'Jermaine Flowers',51,'Perspiciatis et fac',48,70,'Dignissimos in qui q','Lorem aliquip volupt','+1 (296) 507-6769','cyler@mailinator.com','Esse nostrum tempor'),(6,'Carson Lyons',56,'Ea possimus consect',93,75,'Beatae qui autem ad ','Voluptate et consequ','+1 (247) 807-6619','xoliqyjaso@mailinator.com','Eius reiciendis amet'),(7,'Karleigh Waters',71,'Nostrud dolore elige',87,4,'Nobis et proident i','Pariatur Nobis dese','+1 (363) 691-4936','qunodo@mailinator.com','Amet in enim ea in '),(8,'Oscar Huffman',34,'Ad et voluptate culp',73,56,'Rem nihil aspernatur','Quo doloremque cupid','+1 (958) 121-4247','nojyce@mailinator.com','Eos eius iste dolor '),(9,'Raja Jacobson',33,'Totam ad et non veli',26,51,'Quasi similique reru','Magnam exercitation ','+1 (957) 671-9968','jyxubameja@mailinator.com','Aut quos eum rerum e'),(10,'Wallace Price',23,'Dolor cumque irure f',9,45,'Dolorem minim adipis','Qui eiusmod aut volu','+1 (448) 418-5401','xuwylovuj@mailinator.com','Aut incidunt rerum '),(11,'Wallace Price',23,'Dolor cumque irure f',9,45,'Dolorem minim adipis','Qui eiusmod aut volu','+1 (448) 418-5401','xuwylovuj@mailinator.com','Aut incidunt rerum '),(12,'Aileen Kemp',37,'Vero expedita dolor ',25,41,'Eos deleniti reicien','Quae ipsum consequa','+1 (309) 645-2737','vohof@mailinator.com','Tempora exercitation'),(13,'Fuller Humphrey',45,'Modi sed amet aute ',39,16,'Voluptates tempore ','Voluptas aliqua Ill','+1 (193) 273-2707','susedivos@mailinator.com','Maiores repellendus'),(14,'Kasimir Castaneda',17,'Tempore quidem volu',5,66,'Nesciunt quos dicta','Sint est natus dolor','+1 (133) 916-2564','niru@mailinator.com','Tempor neque quos Na'),(15,'Cruz Rojas',42,'Expedita quos quidem',83,15,'Consequatur irure it','Proident error repu','+1 (386) 557-6735','gijynot@mailinator.com','Atque vel cupidatat '),(16,'Chaim Richards',59,'Dolore et proident ',73,29,'Fugiat dicta a perf','Distinctio Animi f','+1 (441) 455-4453','nebiwevofe@mailinator.com','Fugiat modi consequa'),(17,'Brett Nash',100,'Qui laudantium aspe',53,95,'Minim blanditiis opt','Eum temporibus Nam m','+1 (341) 432-5661','xiwugeso@mailinator.com','Est eligendi sit qu'),(18,'Hyatt Briggs',45,'Rerum non aliqua Et',69,51,'Accusamus eos tempor','In corporis unde ut ','+1 (398) 943-6638','dygesa@mailinator.com','Libero iure ex non b'),(19,'Hyatt Briggs',45,'Rerum non aliqua Et',69,51,'Accusamus eos tempor','In corporis unde ut ','+1 (398) 943-6638','dygesa@mailinator.com','Libero iure ex non b'),(20,'Danielle Serrano',10,'Eos est molestias fu',50,9,'Magni suscipit et al','Id ut sit proident','+1 (358) 756-1885','lygacuv@mailinator.com','Aut numquam et aliqu'),(21,'Selma Farrell',78,'Dolores alias volupt',99,90,'Consequatur occaecat','Deleniti delectus a','+1 (478) 695-2219','bigenuq@mailinator.com','Porro nulla officia '),(22,'Taylor Randall',16,'Excepteur aut pariat',79,5,'Est quia exercitati','Voluptates dolorum q','+1 (593) 383-6411','zihyvo@mailinator.com','Esse est incidunt '),(23,'Vivien Small',38,'Assumenda autem erro',72,27,'Quibusdam doloribus ','Laborum soluta lauda','+1 (172) 632-5876','banomataq@mailinator.com','Harum dicta aspernat');
/*!40000 ALTER TABLE `girls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-17  0:05:14
